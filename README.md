# Cordova-Ionic-Todo
A simple todo app built using Cordova, Ionic and AngularJS.

## Getting Started
1. Clone this repo.
2. Install Cordova, Ionic and PhoneGap globally.

        npm install -g cordova ionic
    
3. Install dependencies.

        bower install
        
4. Preview by running

        ionic serve --lab
    
## Real-time Testing
1. Install PhoneGap on your machine.

        npm install -g phonegap
    
2. Install phonegap on your device. You can download it from Google Play, App Store or Window Phone Store.
3. Start PhoneGap on your machine.
        
        phonegap serve
        
4. Open the PhoneGap app on your device and replace the IP address and port with the one supplied by PhoneGap CLI.
5. Tap connect button on your device.
